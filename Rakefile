require 'dromedary/tasks' # require Dromedary gem dependencies
require 'rest-client'
require 'yaml'

config = YAML.load_file("#{Dir.pwd}/config.yml")
app_host = config["#{config['env']}"]
api_credentials = YAML.load_file("#{Dir.pwd}/features/support/fixtures/api_credentials.yml")

# describing Dromedary rake tasks
desc 'Rake task to run all the Dromedary sequence'
task :run_dromedary, :run_on do |task, args|
  ENV["RUN_ON"] = "#{args[:run_on]}"
  %W[prepare_for_a_ride store_cases_titles run_cucumber merge_junit_reports get_case_ids[run] rerun_if_needed generate_cucumber_json_reports create_run[smoke,#{args[:run_on]}] close_run[#{args[:run_on]}] final_clean_ups].each do |task_name|
    sh "rake #{task_name}" do
      #ignore errors
    end
  end
end

desc 'Rake task to run tests with around run cleanups of demo data'
task :gogogo do
  %W[cleanup_demo_data cucumber_simple_run cleanup_demo_data].each do |task_name|
    sh "rake #{task_name}" do
      #ignore errors
    end
  end
end

desc 'Rake task to run tests sequentially in 3 browsers with around run cleanups of demo data'
task :run_sequentially do
  started_at = Time.now
  puts "[35m Started At #{started_at} [0m"

  sh "rake cleanup_demo_data" do
    #ignore errors
  end
  sh "rake run_in_browsers['chrome, firefox, safari']" do
    #ignore errors
  end
  sh "rake cleanup_demo_data" do
    #ignore errors
  end

  finished_at = Time.now
  run_took = Time.at(finished_at - started_at).utc.strftime('%H:%M:%S')
  puts "[35m End at #{finished_at} [0m"
  puts "[35m Run took #{run_took} [0m"
end

desc 'Rake task to run tests in parallel in 3 browsers with around run cleanups of demo data'
task :run_parallel do
  started_at = Time.now
  puts "[35m Started At #{started_at} [0m"

  sh "rake cleanup_demo_data" do
    #ignore errors
  end

  t1 = Thread.new{sh "rake cucumber_simple_run[chrome]"}
  t2 = Thread.new{sh "rake cucumber_simple_run[firefox]"}
  t3 = Thread.new{sh "rake cucumber_simple_run[safari]"}
  t1.join
  t2.join
  t3.join

  sh "rake cleanup_demo_data" do
    #ignore errors
  end

  finished_at = Time.now
  run_took = Time.at(finished_at - started_at).utc.strftime('%H:%M:%S')
  puts "[35m End at #{finished_at} [0m"
  puts "[35m Run took #{run_took} [0m"
end

desc 'Rake task to run tests sequentially in different browsers without any reporting options'
task :run_in_browsers, :browser_1, :browser_2, :browser_3 do |task, args|
  if "#{args[:browser_1]}".empty? && "#{args[:browser_2]}".empty? && "#{args[:browser_3]}".empty?
    puts 'Please specify in what browsers you would like to run tests, e.g. ["chrome, firefox, safari"]'
  else

    unless "#{args[:browser_1]}".empty?
      sh "rake cucumber_simple_run[#{args[:browser_1]}]" do
        #ignore errors
      end
    end

    unless "#{args[:browser_2]}".empty?
      sh "rake cucumber_simple_run[#{args[:browser_2]}]" do
        #ignore errors
      end
    end

    unless "#{args[:browser_3]}".empty?
      sh "rake cucumber_simple_run[#{args[:browser_3]}]" do
        #ignore errors
      end
    end
  end
end

desc 'Simple Cucumber run without any reporting options'
task :cucumber_simple_run, :browser do |task, args|
  case args[:browser]
  when 'chrome'
    ENV['browser'] = 'chrome'
  when 'firefox'
    ENV['browser'] = 'firefox'
  when 'safari'
    ENV['browser'] = 'safari'
  end

  sh "cucumber" do
    #ignore errors
  end
end

desc 'Manual task to delete demo users or projects from remote target server'
task :delete_demo, :instance do |task, args|
  headers = {'Content-Type' => api_credentials['Content-Type'], 'X-Redmine-API-Key' => api_credentials['X-Redmine-API-Key']}
  if "#{args[:instance]}".empty?
    puts 'Please specify what to delete: "users" or "projects" as a rake task argument'
  else
    case args[:instance]
      when 'users'
        instance_limit = 2
        secured_ids = [1, 5155]
      when 'projects'
        instance_limit = 0
        secured_ids = []
      else
        puts 'Only "users" and "projects" argument input is supported for this task'
    end

    while JSON.parse(RestClient.get("#{app_host}#{args[:instance]}.json", headers))['total_count'] != instance_limit
      response = RestClient.get("#{app_host}#{args[:instance]}.json", headers)

      ids = []
      counter ||= 0
      JSON.parse(response)["#{args[:instance]}"].each do |i|
        ids << i['id']
      end
      ids.each do |id|
        unless secured_ids.include? id
          RestClient.delete("#{app_host}#{args[:instance]}/#{id}.json", headers)
          counter +=1
        end
      end
    end

    if counter.nil?
      puts "Zero #{args[:instance]} deleted"
    else
      puts "#{counter} #{args[:instance]} deleted"
    end
    puts "Nothing more to delete in '#{args[:instance]}'"
  end
end

desc 'Demo data cleanup task around each run'
task :cleanup_demo_data do
  %W[delete_demo[users] delete_demo[projects]].each do |task_name|
    sh "rake #{task_name}" do
      #ignore errors
    end
  end
end

desc 'Manual task to delete demo users or projects from remote Gitlab server'
task :delete_demo_gitlab, :instance do |task, args|
  headers = {'Authorization' => api_credentials['GITLAB_API_TOKEN']}
  if "#{args[:instance]}".empty?
    puts 'Please specify what to delete: "users" or "projects" as a rake task argument'
  else
    case args[:instance]
    when 'users'
      secured_ids = [1, 2, 3, 4, 5, 235]
    when 'projects'
      secured_ids = [1]
    else
      puts 'Only "users" and "projects" argument input is supported for this task'
    end

    instance_list = JSON.parse(RestClient.get("#{api_credentials['GITLAB_ROOT_URL']}/#{args[:instance]}", headers))

    id_list_delete = []
    instance_list.each do |i|
      id_list_delete << i['id']
    end

    id_list_delete = id_list_delete - secured_ids # Removing secured IDs from the deletion list

    id_list_delete.each do |id|
      response = RestClient.delete("#{api_credentials['GITLAB_ROOT_URL']}/#{args[:instance]}/#{id}?hard_delete=true", headers)
      raise "Error occurred while deleting #{args[:instance]} with id #{id}" unless (response.code == 202 or response.code == 204)
    end

    if id_list_delete.length == 0
      puts "Zero #{args[:instance]} deleted"
    else
      puts "#{id_list_delete.length} #{args[:instance]} deleted"
    end
    puts "Nothing more to delete in '#{args[:instance]}'"
  end
end

desc 'Demo data cleanup task around each run at Gitlab'
task :cleanup_demo_data_gitlab do
  %W[delete_demo_gitlab[users] delete_demo_gitlab[projects]].each do |task_name|
    sh "rake #{task_name}" do
      #ignore errors
    end
  end
end
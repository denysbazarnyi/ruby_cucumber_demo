Given(/^I am not logged in visitor$/) do
  @home_page = HomePage.new
  @home_page.load

  expect(@home_page.page_header).to have_content STRINGS['Redmine header']
  expect(@home_page.top_menu).to have_login_link
  expect(@home_page.top_menu).to have_register_link
  expect(@home_page.top_menu).not_to have_logout_link
end

When(/^I register '([^"]*)' user via Redmine '([^"]*)'$/) do |user_role, method|
  current_user =  case user_role
                  when 'admin'
                    @admin = User.new "#{user_role}"
                  when 'developer'
                    @developer = User.new "#{user_role}"
                  else
                    raise STRINGS['Case warning']['user roles']
                  end

  case method
  when 'UI'
    @home_page.top_menu.register_link.click
    @registration_page = RegistrationPage.new
    @registration_page.submit_user_data current_user
  when 'API'
    create_user_via_api current_user
  else
    raise STRINGS['Case warning']['registration methods']
  end
end

Then(/^I see the '([^"]*)' user is registered$/) do |user_role|
  case user_role
  when 'admin'
    expect(get_users_email_list).to include @admin.email
  when 'developer'
    expect(get_users_email_list).to include @developer.email
  else
    raise STRINGS['Case warning']['user roles']
  end
end

Then(/^I become logged in as '([^"]*)' user$/) do |user_role|
  @account_page = AccountPage.new

  expect(@account_page.top_menu.logged_user_name).to have_content "#{user_role}"
end

When(/^I create a project$/) do
  @projects_page = ProjectsPage.new
  @projects_page.load

  @projects_page.add_new_project @admin.username + STRINGS['Naming']['project']
end

Then(/^I see that project is created on '([^"]*)' level$/) do |level|
  case level
    when 'UI'
      expect(@projects_page.page_header).to have_content @admin.username + STRINGS['Naming']['project']
      expect(@projects_page).to have_flash_notice
      expect(@projects_page.flash_notice).to have_content STRINGS['Flash notice']['creation']
    when 'API'
      expect(get_projects_name_list).to include @admin.username + STRINGS['Naming']['project']
    else
      raise STRINGS['Case warning']['check levels']
  end
end

When(/^I add '([^"]*)' user as a member of the project$/) do |user_role|
  @projects_page.add_user_as_member @developer
end

Then(/^I can can see '([^"]*)' user in the project member list$/) do |user_role|
  expect(@projects_page.members_table).to have_content @developer.firstname
  expect(@projects_page.members_table).to have_content @developer.lastname
  expect(@projects_page.members_table).to have_content user_role.capitalize
end

When(/^I create an issue and assign '([^"]*)' user to created issue$/) do |user_role|
  @projects_page.create_issue_and_assign_user @developer
end

Then(/^I see the issue is created$/) do
  expect(@projects_page).to have_flash_notice
  expect(@projects_page.flash_notice).to have_content STRINGS['Flash notice']['issue creation 1']
  expect(@projects_page.flash_notice).to have_content STRINGS['Flash notice']['issue creation 2']
end

Then(/^I see '([^"]*)' user is assigned to the issue$/) do |user_role|
  expect(@projects_page.assigned_to).to have_content @developer.firstname
  expect(@projects_page.assigned_to).to have_content @developer.lastname
end

When(/^I logout$/) do
  sleep 0.1
  @projects_page.top_menu.logout
  sleep 0.1
end

When(/^I login as '([^"]*)' user$/) do |user_role|
  case user_role
  when 'admin'
    @home_page.login_as @admin
  when 'developer'
    @home_page.login_as @developer
  else
    raise STRINGS['Case warning']['user roles']
  end
end

When(/^I track time for the assigned issue$/) do
  @home_page.jump_to_first_project
  @projects_page.track_time_for_issue 8
end

Then(/^I see the time is tracked properly$/) do
  expect(@projects_page).to have_flash_notice
  expect(@projects_page.flash_notice).to have_content STRINGS['Flash notice']['creation']
end

When(/^I close the issue$/) do
  @projects_page.close_issue
end

Then(/^I see the issue was closed$/) do
  expect(@projects_page).to have_flash_notice
  expect(@projects_page.flash_notice).to have_content STRINGS['Flash notice']['update']
end

When(/^I close the project$/) do
  @home_page.jump_to_first_project
  @projects_page.close_project
end

Then(/^I see it was successfully closed$/) do
  expect(@projects_page).to have_warning_notice
  expect(@projects_page.warning_notice).to have_content STRINGS['Flash notice']['project closed']
end
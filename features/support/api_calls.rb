require 'rest-client'

module APICalls
  def get_users_email_list
    response = RestClient.get(CONFIG[ENVIRONMENT] + 'users.json', api_headers)

    emails = []
    JSON.parse(response)['users'].each do |u|
      emails << u['mail']
    end
    emails
  end

  def get_projects_name_list
    response = RestClient.get(CONFIG[ENVIRONMENT] + 'projects.json', api_headers)

    names = []
    JSON.parse(response)['projects'].each do |u|
      names << u['name']
    end
    names
  end

  def create_user_via_api(user)
    payload = {
        "user" => {
            "login" => user.username,
            "firstname" => user.firstname,
            "lastname" => user.lastname,
            "mail" => user.email,
            "password" => user.password
        }
    }.to_json
    RestClient.post(CONFIG[ENVIRONMENT] + 'users.json', payload, api_headers)
  end

  def api_headers
    {'Content-Type' => API['Content-Type'], 'X-Redmine-API-Key' => API['X-Redmine-API-Key']}
  end
end

World(APICalls)
require 'cucumber'
require 'rspec/expectations'
require 'selenium-webdriver'
require 'capybara/cucumber'
require 'securerandom'
require 'site_prism'
require 'pry'

require 'yaml'
require 'require_all'

CONFIG = YAML.load_file("#{Dir.pwd}/config.yml")
ENVIRONMENT = ENV['RUN_ON'] || CONFIG['env']
STRINGS = YAML.load_file("#{Dir.pwd}/features/support/fixtures/string_values.yml")
API = YAML.load_file("#{Dir.pwd}/features/support/fixtures/api_credentials.yml")

Capybara.app_host = CONFIG[ENVIRONMENT]

require_all 'page_objects/sections'
require_all 'page_objects/pages'

def chrome?
  ENV['browser'] == 'chrome'
end

def firefox?
  ENV['browser'] == 'firefox'
end

def safari?
  ENV['browser'] == 'safari'
end

# setting Capybara driver
Capybara.default_driver = :selenium
Capybara.register_driver :selenium do |app|
  if chrome?
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: %w[window-size=1800,1000]))
  elsif firefox?
    Capybara::Selenium::Driver.new(app, browser: :firefox)
  elsif safari?
    Capybara::Selenium::Driver.new(app, browser: :safari)
  else
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: Selenium::WebDriver::Chrome::Options.new(args: %w[window-size=1800,1000]))
  end
end

Before do
  # setting Capybara driver to maximize window for Firefox
  Capybara.page.driver.browser.manage.window.maximize
end

After do
  # setting Capybara driver to reset sessions after all tests are done
  Capybara.reset_sessions!
end
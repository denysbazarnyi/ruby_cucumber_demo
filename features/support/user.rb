require 'ffaker'

class User
  attr_accessor :username
  attr_accessor :password
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :email

  def initialize(user_role)
    @password = FFaker::Internet.password
    @firstname = FFaker::Name.first_name
    @lastname = FFaker::Name.last_name
    @email = FFaker::Internet.email
    @username = user_role + '_' + SecureRandom.hex # SecureRandom is required to make sure that user name is unique
  end
end
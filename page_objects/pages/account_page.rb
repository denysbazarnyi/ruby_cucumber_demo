class AccountPage < SitePrism::Page
  set_url 'my/account'

  # ELEMENTS
  section :top_menu, TopMenuSection, '#top-menu'
  section :header,   HeaderSection,  '#header'
  section :sidebar, SidebarSection, '#sidebar'
end
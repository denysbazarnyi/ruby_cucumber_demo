class HomePage < SitePrism::Page
  set_url ''

  # ELEMENTS
  section :top_menu, TopMenuSection, '#top-menu'
  section :header,   HeaderSection,  '#header'

  # METHODS
  def login_as(user)
    top_menu.login_link.click

    @login_page = LoginPage.new
    @login_page.submit_user_credentials user
  end

  def jump_to_first_project
    header.project_jump_dropdown.click
    header.projects.first.click
    sleep 0.5

    @projects_page = ProjectsPage.new
  end

  def page_header
    header.page_header
  end
end
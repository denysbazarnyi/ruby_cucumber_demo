class LoginPage < SitePrism::Page
  set_url 'account/register'

  # ELEMENTS
  section :top_menu, TopMenuSection, '#top-menu'
  section :header,   HeaderSection,  '#header'

  element :user_login_field,                 '#username'
  element :user_password_field,              '#password'
  element :submit_button,                    '#login-submit'

  # METHODS
  def submit_user_credentials(user)
    user_login_field.set user.username
    user_password_field.set user.password

    submit_button.click
  end
end
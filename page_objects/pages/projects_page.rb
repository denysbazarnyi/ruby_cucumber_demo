class ProjectsPage < SitePrism::Page
  set_url 'projects'

  # ELEMENTS
  section   :top_menu,     TopMenuSection,     '#top-menu'
  section   :header,       HeaderSection,      '#header'
  section   :main_menu,    MainMenuSection,    '#main-menu'
  section   :project_tabs, ProjectTabsSection, :xpath, '//*[@id="content"]/div[1]'

  # For new project
  element   :new_project_link,        '.icon-add'
  element   :new_project_name,        '#project_name'
  element   :create_project_button,   :xpath, '//*[@id="new_project"]/input[3]'

  # For existing project
  element   :flash_notice,            '#flash_notice'
  element   :close_project_link,      '.icon-lock'
  element   :warning_notice,          '.warning'
  # Members tab
  element   :new_member_link,         '.icon-add'
  element   :user_search_field,       '#principal_search'
  element   :user_checkbox,           :xpath, '//*[@id="principals"]/label/input'
  element   :developer_role_checkbox, :xpath, '//*[@id="new_membership"]/fieldset[2]/div/label[2]/input'
  element   :add_member_button,       '#member-add-submit'
  element   :members_table,           '.list.members'
  # Issues tab
  element   :new_issue_link,          '.new-issue'
  element   :issue_subject_field,     '#issue_subject'
  element   :issue_selector,          '#issue_assigned_to_id'
  element   :create_issue_button,     :xpath, '//*[@id="issue-form"]/input[3]'
  element   :assigned_to,             '.assigned-to'
  element   :issue_status_selector,   '#issue_status_id'
  element   :submit_changes_button,   :xpath, '//*[@id="issue-form"]/input[6]'
  elements :log_time_links,          :xpath, '//*[@class="icon icon-time-add"]'
  elements :edit_issue_links,        :xpath, '//*[@class="icon icon-edit"]'
  elements :issue_links,             :xpath, '//td[@class="id"]/a'
  # Spent time tab
  element :time_entry_field,          '#time_entry_hours'
  element :activity_selector,         '#time_entry_activity_id'
  element :create_time_entry_button,  :xpath, '//*[@id="new_time_entry"]/input[4]'

  # METHODS
  def add_new_project(project_name)
    new_project_link.click
    sleep 0.5
    new_project_name.set project_name
    create_project_button.click
    sleep 0.5
  end

  def add_user_as_member(user)
    project_tabs.members_tab.click
    new_member_link.click
    user_search_field.set user.firstname + ' ' + user.lastname
    user_checkbox.click
    developer_role_checkbox.click
    add_member_button.click
    sleep 0.5
  end

  def close_project
    close_project_link.click
    page.accept_alert
    sleep 0.5
  end

  def create_issue_and_assign_user(user)
    main_menu.issues_link.click
    new_issue_link.click
    issue_subject_field.set user.username + STRINGS['Naming']['issue']
    issue_selector.select user.firstname + ' ' + user.lastname
    create_issue_button.click
    sleep 0.5
  end

  def track_time_for_issue(time)
    main_menu.issues_link.click
    sleep 0.5
    issue_links.first.click
    log_time_links.first.click

    time_entry_field.set time
    activity_selector.select STRINGS['Selector']['Activity']['development']
    create_time_entry_button.click
    sleep 0.5
  end

  def close_issue
    edit_issue_links.first.click
    issue_status_selector.select STRINGS['Selector']['Status']['closed']
    sleep 0.5
    submit_changes_button.click
    sleep 0.5
  end

  def page_header
    header.page_header
  end
end
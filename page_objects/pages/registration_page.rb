class RegistrationPage < SitePrism::Page
  set_url 'account/register'

  # ELEMENTS
  section :top_menu, TopMenuSection, '#top-menu'
  section :header,   HeaderSection,  '#header'

  element :user_login_field,                 '#user_login'
  element :user_password_field,              '#user_password'
  element :user_confirmation_password_field, '#user_password_confirmation'
  element :user_first_name_field,            '#user_firstname'
  element :user_last_name_field,             '#user_lastname'
  element :user_email_field,                 '#user_mail'
  element :hide_my_email_checkbox,           '#pref_hide_mail'
  element :user_language_select,             '#user_language'
  element :submit_button,                    :xpath, '//*[@id="new_user"]/input[3]'

  # METHODS
  def page_header
    header.page_header
  end

  def submit_user_data(user)
    user_login_field.set user.username
    user_password_field.set user.password
    user_confirmation_password_field.set user.password
    user_first_name_field.set user.firstname
    user_last_name_field.set user.lastname
    user_email_field.set user.email
    user_language_select.select STRINGS['Selector']['User language']['english']

    submit_button.click
    sleep 0.25
  end
end
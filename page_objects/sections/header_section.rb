class HeaderSection < SitePrism::Section

  # ELEMENTS
  element   :page_header,            :xpath, '//*[@id="header"]/h1'
  element   :project_jump_dropdown,  '#project-jump'
  elements :projects,               :xpath, '//*[@id="project-jump"]//a'
end
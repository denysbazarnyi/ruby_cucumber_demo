class ProjectTabsSection < SitePrism::Section

  # ELEMENTS
  element :members_tab, :xpath, '//*[@id="tab-members"]'
end
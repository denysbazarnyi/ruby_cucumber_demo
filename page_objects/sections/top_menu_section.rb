class TopMenuSection < SitePrism::Section

  # ELEMENTS
  element :login_link,      '.login'
  element :register_link,   '.register'
  element :logout_link,     '.logout'
  element :my_account_link, '.my-account'
  element :logged_user_name, '#loggedas'

  # METHODS
  def logout
    logout_link.click
  end
end